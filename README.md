# FRIENDLY DRINK
This is a chatbot which will be able to understand what user inputs in natural language and respond accordingly. This will be written in a combination of Python and Javascript as far as planned.

## Requirements ##
* Python 3
* Any Javascript enabled browser

## Required Packages ##
* Python NLTK (http://www.nltk.org/)
* Python rdflib (https://github.com/RDFLib/rdflib)
* Python wikipedia (>>pip install wikipedia)
* Python JSON 
* Python urllib.request [included in Python 3]

## Database ##
To store the data collected from the chat, we utilize the triple store, Stardog.

* Stardog Community Edition (http://stardog.com/)