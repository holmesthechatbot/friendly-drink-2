$(document).ready(function() {
    messageDisplayer();
});
function messageDisplayer(){
  var messageJSON=
      {
    	"Messages": [{
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:13.980659",
    		"message": "How are you"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:15.887034",
    		"message": "I am fine. How are you?"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:16.913601",
    		"message": "I am good."
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:18.014085",
    		"message": "The weather is a bit dull though."
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:23.433482",
    		"message": "Yeah the weather looks bad"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:24.344787",
    		"message": "It looks like it will be raining the whole day"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:27.795649",
    		"message": "That sucks. Flip a coin"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:28.860260",
    		"message": "How are you"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:30.226455",
    		"message": "How are you"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:31.174324",
    		"message": "How are you"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:32.776039",
    		"message": "How are you"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:33.914232",
    		"message": "How are you"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:35.392347",
    		"message": "How are you"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:36.564572",
    		"message": "How are you"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:37.768981",
    		"message": "How are you"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:39.011025",
    		"message": "How are you"
    	}, {
    		"source": "user",
    		"timestamp": "2016-09-05 16:08:40.160722",
    		"message": "How are you"
    	}, {
    		"source": "computer",
    		"timestamp": "2016-09-05 16:08:41.368214",
    		"message": "How are you"
    	}
    ]
    }
  messageDivCreator(messageJSON)
}

function messageDivCreator(messageJSONObject){
  for (i=0; i<messageJSONObject.Messages.length;i++){
    var bigDiv= document.createElement('div');
    bigDiv.id= "MessageDiv"+(messageJSONObject.Messages[i].timestamp).replace(/\s/g, '');
    bigDiv.className= "Message "+"bubble "+messageJSONObject.Messages[i].source;
    bigDiv.appendChild(document.createTextNode(messageJSONObject.Messages[i].message));
    document.getElementById("container").appendChild(bigDiv);
  }
}
