import nltk # Used for natural language recognition
from nltk.stem import WordNetLemmatizer # Used to lemmatize (find root word)
from nltk.corpus import wordnet # Wordnet for finding synonyms
from nltk.tokenize import sent_tokenize, word_tokenize # Used for tokenization

regExpParserGrammar = '''
NP: {<DT>?((<JJ>*<NN|NNP>*)(<P|PP><NN|NNP>)*)?} # NP
P: {<IN>}           # Preposition
V: {<V.*>}          # Verb
PP: {<P><NP>}      # PP -> P NP
VP: {<V><NP|PP>*}  # VP -> V (NP|PP)*
'''

# Finds synonyms of word - returns array of synonyms
def findSynonyms(entryWord):
    # Array which will contain synonyms
    synonyms = []
    for syn in wordnet.synsets(entryWord):
        synonyms.append(syn.name())
    return synonyms

# Splitting up into sentences
def sentenceChunking(textBlock):
    tokenizedSentences = sent_tokenize(textBlock)
    return tokenizedSentences

# POS tagging provided string, takes in sentence
def partOfSpeechTagging(sentenceString):
    splitSentence = nltk.word_tokenize(sentenceString)
    partOfSpeechTaggedSentence = nltk.pos_tag(splitSentence)
    return partOfSpeechTaggedSentence

# Chunks sentences based on grammar from above
def posChunking(posTaggedSentence):
    cp = nltk.RegexpParser(regExpParserGrammar)
    result = cp.parse(posTaggedSentence)
    result.draw() # Draw graph of POS tags
    return result

# Inputs POS tagged sentence and returns array of all Named Entity Recognition objects
def namedEntityReturn(posTaggedSentence):
    parse_tree = nltk.ne_chunk(posTaggedSentence, binary=True)  # POS tagging before chunking
    print(parse_tree)
    named_entities = []
    for t in parse_tree.subtrees():
        # print(t)
        if t.label() == 'NE':
            named_entities.append(t)
            # named_entities.append(list(t))  # if you want to save a list of tagged words instead of a tree
            # print (named_entities)
    return named_entities
