import urllib.request
from contextlib import closing
import json

def locationWeatherGet ():

    url= 'http://ip-api.com/json'
    with urllib.request.urlopen(url) as response:
        location = json.loads((response.read()).decode('utf-8'))

    url2= 'http://api.openweathermap.org/data/2.5/weather?lat=%7.4f&lon=%7.4f&APPID=c3dae359f90443db69dd233e25b9875d' %(location['lat'], location['lon'])
    with urllib.request.urlopen(url2) as sunny:
        weatherData= json.loads((sunny.read()).decode('utf-8'))


    temp= weatherData['main']
    sky= weatherData['weather'][0]
    wind= weatherData['wind']
    temperature=1.8*(temp['temp']-273)+32

    return (temperature, wind['speed'], sky['id'], str(location['city']),location['query'] )

locationWeatherGet()

#weatherid:
#Group 2xx: Thunderstorm
#Group 3xx: Drizzle
#Group 5xx: Rain
#Group 6xx: Snow
#Group 7xx: Atmosphere:
#    701:mist
#    711:smoke
#    741:fog
#    751:sand
#    761:dust
#    762:volcanic ash
#    771:squalls
#    781:tornado
#Group 800: Clear
#Group 80x: Clouds
#Group 90x: Extreme
# 9xx: Additional
