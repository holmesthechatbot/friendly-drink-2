import json
from datetime import datetime # for using timestamp
import rdflib
# future features: device

# FUNC: send messages to server, overwritten to write to file
def sendMessage(source, message):
    timeStamp = datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f")
    json_data = createJSON(source, timeStamp, message) # JSON string created
    writeToFile(json_data); # Call function to write message to a log - currently a file in local directory

# FUNC: send message to rdf server
# def newRelationship(subject, predicate, object):
#     # ...

# FUNC: return JSON string of full message
def createJSON(source, timestamp, message):
    data = {}
    data["source"] = source
    data["timestamp"] = timestamp
    data["message"] = message
    return data # returns JSON string of source, timestamp and message

# FUNC: receive message

# FUNC: write JSON to file
def writeToFile(json_data):
    with open('message_log.json','a') as outfile: # gets append permission for file
        json.dump(json_data, outfile,sort_keys=True,indent=4,separators=(',',':')) # 'dump's JSON into file
