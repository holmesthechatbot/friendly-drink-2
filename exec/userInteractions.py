import time # Used for time delay function
from dataTransfer import sendMessage # Used for message transfer
from WordNetFunctions import sentenceChunking # Used for tokenization and finding synonyms of words

# Used to reveal text with the time delay
def reveal(text):
    time.sleep(0)
    sendMessage("computer", text)
    print ("FD: " + text)

# Used to get user input and log it
def userInput():
    text = input('> ')
    sendMessage("user",text) # logs user input
    sentenceChunking(text) # not currently doing anything
    return text # returns input

# Used to reveal text without Time Delay and "FD: "
def revealFree(text):
    print (text)
