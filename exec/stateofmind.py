import nltk # Used for natural language recognition
from nltk.stem import WordNetLemmatizer # Used to lemmatize (find root word)
from nltk.corpus import wordnet # Wordnet for finding synonyms
from nltk.tokenize import sent_tokenize, word_tokenize

from WordNetFunctions import findSynonyms

def stateOfMind(sentence):
    fineSynonyms= findSynonyms("fine")+ findSynonyms("good")+findSynonyms("happy")
    sadSynonyms= findSynonyms("sad")+findSynonyms("sick")+findSynonyms("bad")

    for i in range (0, (len(sentence))):
        ki=0
        if (len(sentence))==1:
            li=0
            for syns in fineSynonyms:
                li=1
                if sentence[i][0].lower()==syns:
                    return True
                    break
                if li==0:
                    for syns in sadSynonyms:
                        if sentence[i][0].lower()==syns:
                            return False
                            break

        elif sentence[i][1]=='JJ':
            for syns in fineSynonyms:
                if syns == sentence[i][0].lower():
                    ki=1
                    if i==0:
                        return True
                    else:
                        for k in range (0, (i)):
                            if (sentence[k][0].lower()=="not" or sentence[k][0].lower()=="non"):
                                return False
                            else:
                                continue
                        return True
                    break
            if ki==0:
                for syns in sadSynonyms:
                    if syns == sentence[i][0]:
                        if i==0:
                            return False
                        else:
                            for k in range (0, (i)):
                                if (sentence[k][0].lower()=="not" or sentence[k][0].lower()=="non"):
                                    return True
                                else:
                                    continue
                            return False
                        break



#######################TESTING CONDITIONS#######################################
#str1= "I am good"
#str2="I am not bad"
#str3="Fine"
#str4="I am not doing bad"

#str5="I am sad"
#str6="I am not happy"
#str7="Bad"
#str8="I am not doing well"

#testarr= [str1,str2,str3,str4,str5,str6,str7,str8]

#for i in testarr:
#    if (stateOfMind(nltk.pos_tag(word_tokenize(i)))):
#        print('Happy')
#    else:
#        print('Meh')
